-- Note: I have noticed a lot of scripts that have a loop stop randomly with no warning, so to prevent needing to guess when they failed, I wrote this to give a heads up display:
--    Result is a 36x36px box that blinks between red and black in the lower left corner of the screen, if it stops blinking I know the script died.

local color = true; if(game.Players.LocalPlayer.PlayerGui:FindFirstChild('Pulse')~=nil) then game.Players.LocalPlayer.PlayerGui.Pulse:Destroy(); end; gui = Instance.new('ScreenGui', game.Players.LocalPlayer.PlayerGui); gui.Name = 'Pulse'; local frame = Instance.new('Frame',gui); frame.Size = UDim2.new(0,10,0,10); frame.Position = UDim2.new(0,0,1,-10);
while true do
	wait(0.5);
	if(game.Players.LocalPlayer.PlayerGui:FindFirstChild('Pulse')==nil) then gui = Instance.new('ScreenGui', game.Players.LocalPlayer.PlayerGui); gui.Name = 'Pulse'; frame = Instance.new('Frame',gui); frame.Size = UDim2.new(0,10,0,10); frame.Position = UDim2.new(0,0,1,-10); color = true; end;
	if(color) then color = false; frame.BackgroundColor3 = Color3.fromHSV(1,1,1); else color = true; frame.BackgroundColor3 = Color3.fromHSV(0,0,0); end;
    -- Code that is supposed to run every half second
end;
