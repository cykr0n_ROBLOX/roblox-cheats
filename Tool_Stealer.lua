while true do
    wait(0.5);
    for i,v in pairs(game.Players:GetPlayers()) do
        wait(0.5);
        if(v.Name ~= game.Players.LocalPlayer.Name) then
            for i2, v2 in pairs(workspace[v.Name]:GetChildren()) do
                if v2.ClassName == 'Tool' then
                    if game.Players[game.Players.LocalPlayer.Name].Backpack:FindFirstChild(v2.Name) == nil then --TODO: Fix duplicate tools despite this chck
                        print('Stole '..v2.Name..' from '..v.Name);
                        local t = v2:Clone();
                        t.Parent = game.Players[game.Players.LocalPlayer.Name].Backpack;
                    end;
                end;
            end;
        end;
    end;
end;

--[[ Notes:
        At Vaktovian Recruitment Center: Only a stolen Medigun works. The rest give the model but no functionality. Model is invisible to everyone else, but arm welds are visible.
        At TNI Border: Full functionality except for C4, Hacking Device, and Swords. Model is invisible (to everyone else) and there are no sounds (for everyone else).
]]--