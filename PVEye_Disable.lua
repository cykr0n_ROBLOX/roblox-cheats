print('Loaded PVEye_Disable');
game.Players.LocalPlayer.PlayerGui.ChildAdded:Connect(function(child)
    print('Added to local PlayerGui: '..child.Name);
    if(child.Name == 'Loop' or child.Name == 'Tracker') then
        child.Disabled = true;
    end;
end);

--[[ Notes:
        PVEye persistance works by having two scripts check each other and monitor each others ancestry and property changes,
            however if you disable each script when it is loaded they both fail, and no local PVEye protection is on,
            aka no violation or verification RemoteEvents will fire.
]]--