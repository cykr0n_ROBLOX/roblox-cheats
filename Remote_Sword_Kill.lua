-- First Version
local username = 'cykr0n'; -- Who to target
repeat
	wait(.1);
	workspace[username].Torso.Position = workspace[game.Players.LocalPlayer.Name].Torso.Position + Vector3.new(1,0,-3);
until workspace[username]:FindFirstChildOfClass('Humanoid').Health <= 0;

-- Second Version
local username = 'cykr0n'; -- Who to target
workspace[username].Torso.Anchored = true;
workspace[username].Torso.Position = workspace[game.Players.LocalPlayer.Name].Torso.Position + Vector3.new(1,0,-3);

-- Kill Everyone
for _,v in pairs(game.Players:GetPlayers()) do
	workspace[v.Name].Torso.Anchored = true;
	workspace[v.Name].Torso.Position = workspace[game.Players.LocalPlayer.Name].Torso.Position + Vector3.new(1,0,-3);
end;